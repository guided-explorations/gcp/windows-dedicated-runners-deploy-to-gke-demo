# Step 1 : Create a GKE Cluster

Create a GKE cluster with 1 node through GitLab using the [GKE integration](https://www.youtube.com/watch?v=u3jFf3tTtMk). Here we provisioned a 1 node `n2-standard-1` GitLab managed cluster named `windows-gke`. 

>**Note 1** : Please make sure your GKE Cluster version is at a MINIMUM of **1.14.X**. When GitLab provisions the GKE cluster, you may have to upgrade it manually via CLI or the console which has a button to upgrade any cluster to the desired version. Refer to [this documentation](https://cloud.google.com/kubernetes-engine/docs/how-to/upgrading-a-cluster#upgrade_master) for more information. 

>**Note 2** : Within GitLab, you can also deploy Nginx Ingress Controller to front end your web service.

# Step 2 : Provision a Service Account

Create a service account for gcloud interrogation. 

```powershell

# Create a service account
gcloud iam service-accounts create [SA-NAME] --display-name "[SA-DISPLAY-NAME]"

# Download the service account key and store it in a json file
#     [SA-EMAIL] is [SA-NAME]@[PROJECT-ID].iam.gserviceaccount.com
gcloud iam service-accounts list
gcloud iam service-accounts keys create [PATH-TO-KEY.json] --iam-account [SA-EMAIL]

# Add IAM policy to bind the service account to the editor role
gcloud projects add-iam-policy-binding [PROJECT-ID] --member "serviceAccount:[SA-EMAIL]" --role "roles/editor"

# Activate the service account for gcloud
gcloud auth activate-service-account --key-file=[PATH-TO-KEY.json]

```

# Step 3 : Create a Windows Node Pool

Here we'll provision a Windows instance as a node in our GKE cluster. 

```powershell
# Create a node pool with the WINDOWS_SAC image type and the n1-standard-2 machine type
gcloud container node-pools create windows --cluster=windows-gke --image-type=WINDOWS_SAC --machine-type=n1-standard-2 --num-nodes=1
```

# Step 4 : Configure a Project Specific GitLab Runner

Here we'll configure a Windows instance to function as a GitLab runner so it can pick up CI jobs and execute them in a Windows runtime environment. In this example, we'll be configuring a Windows Container instance that already has Docker installed to run and build docker containers. Below are links for reference for additional details to the subsequent steps below. 

* [Install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html)
* [Registering Runners](https://docs.gitlab.com/runner/register/index.html)
* [Obtaining a Runner Token](https://docs.gitlab.com/ee/ci/runners/#registering-a-specific-runner-with-a-project-registration-token)
* [GitLab Runner Tags](https://docs.gitlab.com/ee/ci/yaml/#tags) -- See Windows example to execute Windows specific jobs on Windows Runners

### Step 4A : Provision an instance for a GitLab Windows Runner 

Provision a *Windows Server version 1909 Datacenter Core for Containers* VM instance in the same region/zone as your GKE cluster. You can also go through the GUI to provision a Windows VM. 

```powershell
gcloud compute instances create [INSTANCE_NAME] --network [NETWORK_NAME] \
   --subnet [SUBNET_NAME] \
   --no-address \
   --zone [ZONE] \
   --image-project [GCP-PROJECT] \
   --image-family [IMAGE_FAMILY] \
   --machine-type [MACHINE_TYPE] \
   --boot-disk-size [BOOT_DISK_SIZE] \
   --boot-disk-type [BOOT_DISK_TYPE]
```

### Step 4B-1 : Download and Register the GitLab Runner 

More detailed instructions [here](https://docs.gitlab.com/runner/install/windows.html)

RDP into the VM instance and you should be prompted with CMD terminal. 

```powershell
# Switch to PowerShell command line
powershell

# Make a directory to save your GitLab runner binary
mkdir C:\GitLab-Runner

# Change into that directory
cd C:\GitLab-Runner

# Download the GitLab Binary (https://docs.gitlab.com/runner/install/bleeding-edge.html#download-any-other-tagged-release)
Invoke-WebRequest -Uri "https://s3.amazonaws.com/gitlab-runner-downloads/master/binaries/gitlab-runner-windows-amd64.exe" -OutFile "C:\GitLab-Runner\gitlab-runner.exe"

# Run an elevated Admin Powershell command line 
Start-Process PowerShell -Verb RunAs

# In the new PS window, make sure you're still in C:\GitLab-Runner, execute the following command to register the GitLab Runner:
./gitlab-runner.exe register

# Enter in your GitLab Domain. in this case, we're using GitLab.com
https://gitlab.com/

# When prompted for a token, navigate to Settings > CI/CD > Runners. Refer to these instructions as needed: https://docs.gitlab.com/ee/ci/runners/ and enter it into the PS Command Line
mHB...redacted...2qAZCo

# When prompted for a description, enter in details for human reference
gitlab windows runner

# When prompted to enter gitlab-ci tags for this runner (comma separated) enter in the following:
windows

# Registration should have succeeded and now we will specify the default executor for this runner as shell for windows which will automatically set Powershell as the default shell environment. 
shell
```

### Step 4B-2 : 

Here we'll set up the service in the background to run headless when the instance reboots for any reason. 

```powershell

# Stop any running process of the Runner service

.\gitlab-runner.exe stop

# Install the runner service with the Built-In System Account
.\gitlab-runner.exe install

# Start the runner service 
.\gitlab-runner.exe Start

# Validate the service is running in the background
Get-Process
```

### Step 4C : Install Kubectl and Git

Here we'll install kubectl so Powershell Runtime environments will have the appropriate CLI to deploy to Kuberentes cluster. 

```powershell
# Install the kubectl install script from PowerShell Gallery
Install-Script -Name install-kubectl -Scope CurrentUser -Force

# Install Kubectl binary
install-kubectl.ps1 -DownloadLocation C:\Windows\System32\

# Add Kubectl to your PATH variable
setx path "%path%;C:\Windows\System32\kubectl.exe"

# Install git cli from Powershell Gallary
Install-Script -Name Install-Git

# If prompted, save the script to default location: C:\Program Files\WindowsPowerShell\Scripts Change directory to where you downloaded the install script and run the script
cd C:\Program Files\WindowsPowerShell\Scripts
.\Install-Git.ps1

# Add git to your PATH variable
setx path "%path%;C:\Program Files\Git\bin\git.exe"

# Exit and re-enter the shell and validate kubectl and git is installed correctly.
kubectl.exe version
git.exe -h
```

# Step 5 : Kick off CI/CD pipeline to deploy to a Windows node in GKE

Here we'll update the code and kick off a pipeline defined in [`.gitlab-ci.yml`](./.gitlab-ci.yml). Basically the pipeline will build a simple IIS web page hosted in a single docker container. Then the pipeline will use `kubectl` to deploy the service as defined in the `deployment.yml` file. 

To kick off a pipeline, simply commit any changes to this `README.MD` file and push to this project. 


# Notes

To ensure your pods are correctly scheduled onto Windows nodes, you will need to add a node selector to your pod specification within your [`deployment.yml`](./deployment.yml#L17) file.

```yaml
nodeSelector:
  kubernetes.io/os: windows
```

To ensure your CI Runtime environment supports building and executing Windows applications, ensure you have a Windows Runner configured as per Step 4 above and that your [`.gitlab-ci.yml`](./.gitlab-ci.yml#L7) file has right label annotation `windows` identifying which runners can execute the job. 

```yaml
ci_job:
  tags:
    - windows
 ```
